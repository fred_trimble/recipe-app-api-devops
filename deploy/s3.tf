resource "aws_s3_bucket" "app_public_files_ft_ke" {
  bucket        = "${local.prefix}-files-ft-ke"
  acl           = "public-read"
  force_destroy = true
}
